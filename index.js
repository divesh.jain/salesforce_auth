const express = require("express");
const jsforce = require("jsforce");
var path = require("path");

const app = express();
const port = 3000;
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
var session = require("express-session");
app.use(session({ secret: "this-is-a-secret-token", tokens: "" }));

app.use(express.json());
require("dotenv").config();

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/login", function (req, res) {
  const oauth2 = new jsforce.OAuth2({
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET_ID,
    redirectUri: `${req.protocol}://${req.get("host")}/${
      process.env.REDIRECT_URI
    }`,
  });
  res.render("login", {
    authorize_uri: oauth2.getAuthorizationUrl(),
    redirect_uri: `${req.protocol}://${req.get("host")}/${
      process.env.REDIRECT_URI
    }`,
  });
  // res.redirect(oauth2.getAuthorizationUrl({}));
});

app.get("/getAccessToken", function (req, res) {
  const oauth2 = new jsforce.OAuth2({
    clientId: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET_ID,
    redirectUri: `${req.protocol}://${req.get("host")}/${
      process.env.REDIRECT_URI
    }`,
  });
  const conn = new jsforce.Connection({ oauth2: oauth2 });
  conn.authorize(req.query.code, function (err, userInfo) {
    if (err) {
      return console.error(err);
    }
    req.session.token = conn.accessToken;
    req.session.refreshToken = conn.refreshToken;
    req.session.url = conn.instanceUrl;
    res.send("login success");
  });
});

app.get("/logout", function (req, res) {
  if (req.session.tokens != undefined) {
    req.session.token = null;
    req.session.url = null;
  }
  res.redirect("/");
});

app.get("/test", function (req, resp) {
  if (req.session.token != undefined) {
    console.log("Refresh token is: " + req.session.refreshToken);
    console.log("instance url is: " + req.session.url);
    const conn2 = new jsforce.Connection({
      instanceUrl: req.session.url,
      accessToken: req.session.token,
    });
    var detail = [];
    var record = [];
    conn2.identity(function (err, res) {
      var details = {};
      if (err) {
        return console.error(err);
      }
      console.log("user ID: " + res.user_id);
      console.log("organization ID: " + res.organization_id);
      console.log("username: " + res.username);
      console.log("display name: " + res.display_name);
      details["userId"] = res.user_id;
      details["organizationId"] = res.organization_id;
      details["username"] = res.username;
      details["displayName"] = res.display_name;
      detail.push(details);
      console.log("from server itself: " + JSON.stringify(detail[0]));
      resp.render("test", {
        detail: JSON.stringify(detail[0]),
      });
    });

    // conn2.query("SELECT Id, Name FROM Account", function (err, result) {
    //   var records = {};
    //   if (err) {
    //     return console.error(err);
    //   }
    //   console.log("total : " + result.totalSize);
    //   console.log("fetched : " + result.records.length);
    //   console.log("done ? : " + result.done);
    //   records["totalSize"] = result.totalSize;
    //   records["fetched"] = result.records.length;
    //   if (!result.done) {
    //     console.log("next records URL : " + result.nextRecordsUrl);
    //   }
    //   record.push(records);
    //   console.log("from server itself: " + JSON.stringify(record[0]));
    // });
    // console.log("=======");
  } else {
    resp.redirect("/");
  }
});

app.get("/getAccountDetails", function (req, res) {
  const conn = new jsforce.Connection({
    instanceUrl: req.session.url,
    accessToken: req.session.token,
  });
  conn.query("SELECT Id, Name FROM Account", function (err, result) {
    if (err) {
      res.send(err);
      return;
    }
    res.send(result);
  });
});

app.get("/getUserDetails", function (req, res) {
  const conn = new jsforce.Connection({
    instanceUrl: req.session.url,
    accessToken: req.session.token,
  });
  conn.query("SELECT Id, Name, Profile.Name FROM User", function (err, result) {
    if (err) {
      res.send(err);
      return;
    }
    res.send(result);
  });
});

app.get("/getOpportunities", function (req, res) {
  const conn = new jsforce.Connection({
    instanceUrl: req.session.url,
    accessToken: req.session.token,
  });
  conn.query(
    "SELECT Id, Name, Account.Name FROM Opportunity",
    function (err, result) {
      if (err) {
        res.send(err);
        return;
      }
      res.send(result);
    }
  );
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
